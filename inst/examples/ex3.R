library(landscapegenerator)
library(raster)
library(tmap)

input_raster = raster("/home/jakub/Science/sil/configurational_entropy/resources/LG/LG/examples/input/lg_input_ls_1_example_1.asc")

output_raster = generate_landscape(input_raster,
                                   improvement_thershold = "2000000",
                                   parcel_basic_type = "35.0,65.0",
                                   parcel_image_cell_size = 1,
                                   parcel_image_lines = "false",
                                   orderedcriteria_solvemode = "sequentially",
                                   criteria_classes = "criteria.classes=ClusterCriteria 1,3;\\ClusterCriteria 2,1;\\GlobalPerimeterCriteria 1,246;\\AreaCriteria 1,0,33,1;\\AreaCriteria 1,1,33,1;\\AreaCriteria 1,2,33,1;\\")

plot(output_raster)
